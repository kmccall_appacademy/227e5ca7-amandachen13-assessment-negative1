# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  min = nums.first
  max = nums.last
  (min...max).reject {|num| nums.include?(num)}
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  digits = binary.to_s.chars.reverse
  power = 0
  sum = 0

  digits.each do |digit|
    sum += digit.to_i * (2**power)
    power += 1
  end

  sum
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    selected_pairs = {}
    self.each do |k, v|
      selected_pairs[k] = v if prc.call(k, v)
    end
    selected_pairs
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    merge_hash = {}
    self.each do |k, v|
      merge_hash[k] = v
    end
    hash.each do |k, v|
      if prc && self.key?(k)
        merge_hash[k] = prc.call(k, self[k], hash[k])
      else
        merge_hash[k] = v
      end
    end
    merge_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

# def lucas_numbers(n)
#   if n == 0
#     return 2
#   elsif n == 1
#     return 1
#   elsif n < 0
#     return lucas_numbers(n + 2) - lucas_numbers(n + 1)
#   else
#     return lucas_numbers(n - 2) + lucas_numbers(n - 1)
#   end
# end

def lucas_numbers(n)
  return 2 - n if n == 0 || n == 1

  a = 2
  b = 1
  (n.abs - 1).times {x = a; a = b; b = x + b}

  return -b if n < 0 && n.odd?
  b
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  palindromes = []
  letters = string.chars

  letters.each_with_index do |letter, idx1|
    next if string.count(letter) == 1
    indices(string, letter).each do |idx2|
      next if idx2 < idx1
      check = string[idx1..idx2]
      palindromes << check if palindrome?(check)
    end
  end

  return false if palindromes.empty?

  longest = palindromes.sort_by {|pal| pal.length}.last
  if longest.length <= 2
    return false
  else
    return longest.length
  end
end

def indices(string, letter)
  idx_array = []
  string.each_char.with_index do |ch, idx|
    idx_array << idx if ch == letter
  end
  idx_array
end

def palindrome?(string)
  string == string.reverse
end
